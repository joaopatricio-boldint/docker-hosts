# README #
These scripts allow to refer to a docker ip by it's name

### How it works? ###

1. Make a private backup of your hosts file ``sudo cp /etc/hosts ~/hosts.bkp``
2. Make the directory ``sudo mkdir -p /etc/hosts.d/``
3. Copy the current hosts ``sudo cp /etc/hosts /etc/hosts.d/hosts``
4. ``cd {$repo_dir}``
5. ``chmod +x ./make-docker-hosts ./rebuild-hosts``
5. ``sudo ./make-docker-hosts``

After run the make-docker-hosts script, if a docker with the name www-01 has the ip 172.0.0.1, we can call http://www-01 to access the docker

### Who do I talk to? ###

* Joao Patricio